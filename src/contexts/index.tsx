import { ChakraProvider } from "@chakra-ui/react";
import { ReactNode } from "react";
import { TintasProvider } from "./Tintas";
import { ColorsProvider } from "./Colors";

import theme from "../style/theme";

interface IAppProviderProps {
  children: ReactNode;
}

export const AppProvider = ({ children }: IAppProviderProps) => (
  <ColorsProvider>
    <TintasProvider>
      <ChakraProvider theme={theme}>{children}</ChakraProvider>
    </TintasProvider>
  </ColorsProvider>
);
