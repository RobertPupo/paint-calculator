import { createContext, ReactNode } from "react";
import { DataTintas } from "./tintas";
interface Tintas {
  title: string;
  redimentoEmLitros: number;
}

interface TintasProviderProps {
  children: ReactNode;
}

interface TintasProviderData {
  tintas: Tintas[];
}

export const TintasContext = createContext<TintasProviderData>(
  {} as TintasProviderData
);

export const TintasProvider = ({ children }: TintasProviderProps) => {
  const tintas = DataTintas;
  return (
    <TintasContext.Provider value={{ tintas }}>
      {children}
    </TintasContext.Provider>
  );
};
