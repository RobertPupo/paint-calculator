export const BoxColors = [
  { name: "BoxColors.Red", title: "Red" },
  { name: "BoxColors.Yellow", title: "Yellow" },
  { name: "BoxColors.LightBlue", title: "Light Blue" },
  { name: "BoxColors.DarkBlue", title: "Dark Blue" },
  { name: "BoxColors.Purple", title: "Purple" },
  { name: "BoxColors.Pink", title: "Pink" },
  { name: "BoxColors.Gray", title: "Gray" },
  { name: "BoxColors.Green", title: "Green" },
  { name: "BoxColors.GreenApple", title: "Green Apple" },
  { name: "BoxColors.BabyBlue", title: "Bay Blue" },
  { name: "BoxColors.RedWine", title: "Red Wine" },
  { name: "BoxColors.Black", title: "Black" },
  { name: "BoxColors.Orange", title: "Black" },
  { name: "BoxColors.Beterraba", title: "Beterraba" },
  { name: "BoxColors.VerdeGrama", title: "Verde Grama" },
  { name: "BoxColors.Creme", title: "Creme" },
  { name: "BoxColors.Palha", title: "Palha" },
  { name: "BoxColors.LightPink", title: "Light Pink" },
  { name: "BoxColors.Yellow", title: "Yellow" },
  { name: "BoxColors.LightBlue", title: "Light Blue" },
];
