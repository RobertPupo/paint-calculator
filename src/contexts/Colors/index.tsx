import { BoxColors } from "./colors";
import { createContext, ReactNode } from "react";

interface Colors {
  name: string;
  title: string;
}

interface ColorsProviderProps {
  children: ReactNode;
}

interface ColorsProviderData {
  colors: Colors[];
}

export const ColorsContext = createContext<ColorsProviderData>(
  {} as ColorsProviderData
);

export const ColorsProvider = ({ children }: ColorsProviderProps) => {
  const colors = BoxColors;
  return (
    <ColorsContext.Provider value={{ colors }}>
      {children}
    </ColorsContext.Provider>
  );
};
