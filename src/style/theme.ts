import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  colors: {
    primary: {
      main: "#FFFF",
      main1: "#0A0404",
    },
    gray: {
      0: "#f5f5f5",
      10: "#F2ECEC",
      50: "#F4EAEA",
      100: "#e0e0e0",
      300: "#828282",
      600: "#464646",
    },
    avatar: {
      main: "#FFEBA3",
    },
    destak: {
      main: "#00B5B5",
      main1: "#F5FCFF",
    },
    BoxColors: {
      Red: "#F60F0F",
      Yellow: "#EFCC45",
      LightBlue: "#00B3DA",
      DarkBlue: "#0749AC",
      Purple: "#A910C2",
      Pink: "#F60CDF",
      Gray: "#464646",
      Green: "#54B469",
      GreenApple: "#56D807",
      BabyBlue: "#5FEBF4",
      RedWine: "#A00808",
      Black: "#090000",
      Orange: "#F59607",
      DarkPurple: "#530270",
      Brown: "#5302rr",
      Beterraba: "#530270",
      VerdeGrama: "#090",
      Creme: "#FFEBA3",
      AzulMarinho: "#00B5",
      Palha: "#A91",
      LightPink: "#F60CDF",
    },
  },
  textStyles: {
    heading: {
      fontSize: "16px",
      fontWeight: "regular",
    },
    body: {
      fontSize: "14px",
      fontWeight: "regular",
    },
  },
  fonts: {
    heading: "Monoton",
    body: "Roboto",
  },
  styles: {
    global: {
      body: {
        bg: "primary.main",
        color: "gray.600",
      },
    },
  },
});

export default theme;
