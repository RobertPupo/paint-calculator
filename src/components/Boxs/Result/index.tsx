import { Box, Text, VStack, Heading, useMediaQuery } from "@chakra-ui/react";

interface ResulCardProps {
  result: number;
}

export const Result = ({ result }: ResulCardProps) => {
  const [isLargerThan769] = useMediaQuery("(min-width: 769px)");
  return (
    <>
      {isLargerThan769 ? (
        <Box>
          <VStack>
            <Heading as="h4">Resultado</Heading>
            <Box
              w={"500px"}
              h={"100px"}
              boxShadow={"lg"}
              mb={"50px"}
              _hover={{
                transform: "translateY(-3px)",
                borderColor: "BoxColors.Yellow",
              }}
              transition="border 0.2s, ease 0s, transform 0.2s"
            >
              <Text fontSize={"20px"} fontWeight="bold" color="primary.main1">
                {""}
              </Text>
            </Box>
            <Heading as="h4" mt={"10px"}>
              Produtos sugeridos
            </Heading>
            <Box
              w={"600px"}
              h={"200px"}
              boxShadow={"md"}
              mb={"50px"}
              _hover={{
                transform: "translateY(-3px)",
                borderColor: "BoxColors.Yellow",
              }}
              transition="border 0.2s, ease 0s, transform 0.2s"
            ></Box>
          </VStack>
        </Box>
      ) : (
        /* mobile */
        <Box ml={"35px"}>
          <VStack>
            <Heading as="h4" mt={"20px"}>
              Resultado
            </Heading>
            <Box
              w={"300px"}
              h={"100px"}
              border={"1px"}
              borderColor={"gray.100"}
              boxShadow={"lg"}
              borderRadius={"10"}
              mb={"50px"}
              _hover={{
                transform: "translateY(-3px)",
                borderColor: "BoxColors.Yellow",
              }}
              transition="border 0.2s, ease 0s, transform 0.2s"
            >
              <Text fontSize={"20px"} fontWeight="bold" color="primary.main1">
                {"Você vai precisar de"}
              </Text>
              <Text fontSize={"20px"} fontWeight="bold" color="primary.main1">
                Lata de {result} +
              </Text>
            </Box>
            <Heading as="h4">Produtos sugeridos</Heading>
            <Box
              w={"300px"}
              mb={"50px"}
              flexDirection={"column"}
              justifyContent={"space-between"}
              flexWrap={"wrap"}
            ></Box>
          </VStack>
        </Box>
      )}
    </>
  );
};
