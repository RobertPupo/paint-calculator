import { Text, HStack, Image, useMediaQuery } from "@chakra-ui/react";

import avatarDoubt from "../../../assets/images/boxDialogs/avatarDoubt.svg";
import avatarDialog from "../../../assets/images/boxDialogs/avatarDialog.svg";
import avatarWalker from "../../../assets/images/boxDialogs/avatarWalker.svg";
import avatarComputer from "../../../assets/images/boxDialogs/avatarComputer.svg";
import boxDialog from "../../../assets/images/boxDialogs/boxDialog.png";

export const AvatarQuestion = () => {
  const [isLargerThan769] = useMediaQuery("(min-width: 769px)");

  return (
    <>
      {isLargerThan769 ? (
        <>
          <HStack spacing={"-140px"}>
            <Image
              src={avatarDoubt}
              alt="avatar duvida"
              w={"280px"}
              h={"380px"}
              mt={"320px"}
            />

            <HStack spacing={"-130px"}>
              <Image src={boxDialog} w={"150px"} h={"110px"} />
              <Text
                color="primary.main"
                w={"120px"}
                textAlign={"center"}
                fontSize={"12px"}
              >
                {"De quanta tinta vamos precisar para fazer nossa pintura?"}
              </Text>
            </HStack>
          </HStack>
        </>
      ) : (
        <></>
      )}
    </>
  );
};

export const AvatarAnswer = () => {
  const [isLargerThan769] = useMediaQuery("(min-width: 769px)");

  return (
    <>
      {isLargerThan769 ? (
        <>
          <HStack spacing={"-130px"}>
            <Image
              src={avatarDialog}
              alt="avatar dialogo"
              w={"260px"}
              h={"360px"}
              mt={"300px"}
            />
            <Image src={boxDialog} w={"150px"} h={"110px"} />
            <Text
              color="primary.main"
              w={"120px"}
              textAlign={"center"}
              fontSize={"12px"}
            >
              {"Bora usar a calculadora de tintas para saber!"}
            </Text>
          </HStack>
        </>
      ) : (
        <></>
      )}
    </>
  );
};

export const Tips = () => {
  const [isLargerThan821] = useMediaQuery("(min-width: 821px)");

  return (
    <>
      {isLargerThan821 ? (
        <>
          <HStack spacing={"-30px"} mt={"20px"}>
            <Image
              src={avatarWalker}
              alt="avatar walker"
              w={"360px"}
              h={"460px"}
            />

            <Text
              color="primary.main1"
              w={"300px"}
              textAlign={"justify"}
              fontSize={"18px"}
            >
              Dicas do pintor ;)
              <ul>
                <li>
                  Além da tinta considere também outros materiais como lixas,
                  pincéis, rolos, massa acrilica e etc..
                </li>
                <li>
                  Se a cor atual for mais escura que a cor escolhida considere
                  uma demão a mais.
                </li>
                <li>
                  Caso queira combinar tons procure não pintar muitas paredes
                  com cores escuras para não dar impressão de ambiente menor.
                </li>
                <li>
                  Caso suas paredes tenham buracos e infiltrações será
                  necessário arrumar antes de pintar
                </li>
              </ul>
            </Text>
            <Image
              src={avatarComputer}
              alt="avatar computer"
              w={"360px"}
              h={"460px"}
            />
          </HStack>
        </>
      ) : (
        <></>
      )}
    </>
  );
};
