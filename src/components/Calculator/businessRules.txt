O que deve ter a aplicação:

1 - opção para o usuário inserir medidas de cada parede (máximo de 4 paredes) (ok)
2 - quantas janelas e portas por parede (ok)
3 - Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, (ok)
sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L

-----------------------------------------------------------------------------------------------------------------------------

Regras de negocio:

1 - Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 15 metros quadrados, 
mas podem possuir alturas e larguras diferentes.(ok)

2 - O total de área das portas e janelas deve ser no máximo 50% da área da parede.(ok)

3 - A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta.(ok)

4 - Cada janela possui as medidas: 2,00 x 1,20 mtos (ok)

5 - Cada porta possui as medidas: 0,80 x 1,90 (ok)

6 - Cada litro de tinta é capaz de pintar 5 metros quadrados (ok)
(obs) => {Não considerar teto nem piso}

As variações de tamanho das latas de tinta são:

0,5 L pinta 2,5 m2
2,5 L pinta 12,5 m2
3,6 L pinta 18 m2
18 L pinta 90 m2

supondo que a quantidade total de litros é de totalTintaNecessaria = 6:

Algoritmo:

- ele vai comparar no array de tintas se existe alguma lata que tenha a medida > que "totalTintaNecessaria"
- ele vai receber todos os valores e retirar o que está fora da medida.
- ele vai retirar do array o maior número e nesse momento vai setar esse elemento no array de recomendação.
- ele vai então tirar a "diferença" entre esse número e o "totalTintaNecessaria"
- "totalTintaNecessaria" = diferença
- ele vai repetir todo metodo até o array de tintas estar vazio, caso totalTintaNecessaria > 0
setar 1 para tinta de 0.5 no array de recomendação

