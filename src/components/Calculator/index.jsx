import {
  Box,
  Text,
  VStack,
  Input,
  Button,
  FormControl,
  FormLabel,
  FormErrorMessage,
  FormHelperText,
  Select,
  HStack,
  Heading,
} from "@chakra-ui/react";

import { AvatarQuestion, AvatarAnswer, Tips } from "../Boxs/TopDialogBox";

import { useState } from "react";

export const PaintCalculator = () => {
  /* variavel que pega o valor da altura da parede */
  const [inputAltura, setInputAltura] = useState(0);
  const handleInputAlturaValue = (e) => setInputAltura(e.target.value);

  /* variavel que pega o valor da largura da parede */
  const [inputLargura, setInputLargura] = useState(0);
  const handleInputLarguraValue = (e) => setInputLargura(e.target.value);

  /* variavel que pega a área de cada parede */
  const areaParede = inputAltura * inputLargura;
  const isErrorAreaParede = areaParede < 1 || areaParede > 15;

  /* variavel que pega a quantidade de portas */
  const [inputPorta, setInputPorta] = useState(0);
  const handleInputPortaValue = (e) => setInputPorta(e.target.value);
  const areaPorta = inputPorta * (0.8 * 1.9);

  /* variavel que pega a quantidade de janelas */
  const [inputJanela, setInputJanela] = useState(0);
  const handleInputJanelaValue = (e) => setInputJanela(e.target.value);
  const areaJanela = inputJanela * (2 * 1.2);

  /* soma total da area das janelas e portas */
  const totalAreaJanelaPorta = areaPorta + areaJanela;

  /* tratando erros */

  /* verifica se há valores no input altura e largura respectivamente */
  const isErrorAltura = inputAltura === "";
  const isErrorLargura = inputLargura === "";

  /* verifica se o total da soma das aréas das portas 
  e janelas é maior ou igual a área da parede  e se tem 
  pelo menos uma porta ou janela*/

  const isErrorAreaPortaJanela =
    totalAreaJanelaPorta > areaParede / 2 &&
    (inputPorta > 0 || inputJanela > 0);

  /*  verifica se a altura da parede é o maior ou igual a 2.20 
  e se tem pelo menos 1 porta */
  const isErrorAlturaParedePorta = inputAltura < 2.2 && inputPorta !== 0;

  /* verifica se está tudo certo */

  const SomeError =
    (isErrorAreaPortaJanela && isErrorAlturaParedePorta) ||
    isErrorAreaPortaJanela ||
    isErrorAlturaParedePorta;
  const isSomeError = !SomeError;

  /* verificando metragem total final */

  const MetragemTotalParede = areaParede - totalAreaJanelaPorta;

  /* armazenando informações da parede */

  const [paredes, setParedes] = useState([
    { metragemTotalParede: MetragemTotalParede },
  ]);

  const AddParede = () => {
    setParedes([
      ...paredes,
      {
        altura: Number(inputAltura),
        largura: Number(inputLargura),
        portasQTD: inputPorta,
        janelaQTD: inputJanela,
        areaParede: areaParede,
        areaPortaJanela: totalAreaJanelaPorta,
        metragemTotalParede: MetragemTotalParede,
      },
    ]);
  };

  const areaParedes = paredes
    .map((parede) => parede.metragemTotalParede)
    .reduce(function (acumulador, valorAtual) {
      return acumulador + valorAtual;
    });

  /* validando informações */
  const [isActive, setIsActive] = useState(false);
  const [isActiveColor, setIsActiveColor] = useState("BoxColors.Green");

  const Verify = () => {
    if (paredes.length >= 4) {
      setIsActive(true);
      setIsActiveColor("BoxColors.Red");
    }
  };

  /* particionando area total a ser pintada por lata de tinta */

  let totalTintaNecessaria = areaParedes / 5;
  console.log(totalTintaNecessaria);

  const arrayRecomendacao = [];
  const tintasCopy = [0.5, 2.5, 3.6, 18];

  while (tintasCopy.length > 0) {
    const encontrandoValoresMaiores = tintasCopy.map(
      (quantidadeLitros) => quantidadeLitros > totalTintaNecessaria
    );

    console.log(tintasCopy.length);
    console.log(tintasCopy);
    console.log(encontrandoValoresMaiores);
    const index = tintasCopy.length - 1;
    const retornoExclusao = encontrandoValoresMaiores[index];
    console.log(retornoExclusao);

    if (retornoExclusao !== true) {
      const valorRecomendacao = tintasCopy[index];
      arrayRecomendacao.push(valorRecomendacao);
      totalTintaNecessaria = totalTintaNecessaria - valorRecomendacao;
    } else {
      const removeMaiorqueNecessario = tintasCopy.splice(index, 1);
    }
  }

  console.log(arrayRecomendacao);

  /*  Tratando resultado */

  /* Pegando quantidade de cada lata */

  const discoveryQtd = (value) => {
    const arr = arrayRecomendacao;
    const arrQtd = [];
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] === value) {
        arrQtd.push(arr[i]);
      }
    }
    return arrQtd.length;
  };

  const lataPnq = "Lata de 0.5ml : " + discoveryQtd(0.5);
  const lataMedia = "Lata de 2.5 L  : " + discoveryQtd(2.5);
  const lataGrande = "Lata de 3.6 L  : " + discoveryQtd(3.6);
  const lataGigante = "Lata de 18 L   : " + discoveryQtd(18);

  const result = [lataPnq, lataMedia, lataGrande, lataGigante];

  return (
    <>
      <VStack alignItems={"center"}>
        <HStack spacing={"70px"}>
          <AvatarQuestion />

          <FormControl
            ml={"40px"}
            w={"350px"}
            border={"1px"}
            borderColor={"gray.100"}
            boxShadow={"lg"}
            borderRadius={"10"}
            mb={"50px"}
            _hover={{
              transform: "translateY(-3px)",
              borderColor: "BoxColors.Yellow",
            }}
            transition="border 0.2s, ease 0s, transform 0.2s"
            alignItems={"center"}
          >
            <Text fontWeight={"bold"} mt={"10px"}>
              Calculadora de Tintas
            </Text>
            <FormLabel
              htmlFor="text"
              ml={"30px"}
              mt={"20px"}
              fontWeight={"bold"}
            >
              O que você vai pintar ?
            </FormLabel>
            <Select
              id="options"
              placeholder="Selecione"
              w={"300px"}
              ml={"30px"}
              boxShadow={"md"}
            >
              <option>Paredes</option>
              <option>Pisos</option>
              <option>Tetos</option>
              <option>Outros</option>
            </Select>

            <FormLabel
              htmlFor="text"
              ml={"30px"}
              mt={"10px"}
              fontWeight={"bold"}
            >
              Qual a superficie ?
            </FormLabel>
            <Select
              id="options"
              placeholder="Selecione"
              w={"300px"}
              ml={"30px"}
              boxShadow={"md"}
            >
              <option>Alvenaria</option>
              <option>Gesso</option>
              <option>Madeira</option>
              <option>Metal</option>
              <option>Pedras Naturais</option>
              <option>Telhas</option>
              <option>Tijolos</option>
            </Select>

            <FormLabel
              htmlFor="text"
              ml={"30px"}
              mt={"10px"}
              fontWeight={"bold"}
            >
              Informe as dimensões e a quantidade de janelas e portas de cada
              parede (deve ser 4):
            </FormLabel>
            <Text>Paredes até agora : {paredes.length - 1}</Text>
            <Box>
              {!isErrorAreaParede ? (
                <FormHelperText fontSize={"15px"}>
                  Área deve ser maior 1m2 e menor que 15m2
                </FormHelperText>
              ) : (
                <Text fontSize={"15px"} color={"BoxColors.Red"}>
                  Área é menor que 1m2 ou maior que 15m2
                </Text>
              )}
            </Box>
            <HStack spacing={"1"}>
              <FormControl isInvalid={isErrorLargura}>
                <VStack>
                  <Input
                    placeholder="L (m)"
                    w={"90px"}
                    boxShadow={"md"}
                    value={inputLargura}
                    onChange={handleInputLarguraValue}
                    type="number"
                    isRequired={"true"}
                  />
                  {!isErrorLargura ? (
                    <FormHelperText>inserir largura</FormHelperText>
                  ) : (
                    <FormErrorMessage>largura obrigatória.</FormErrorMessage>
                  )}
                </VStack>
              </FormControl>

              <Box
                fontSize={"30px"}
                fontWeight={"extrabold"}
                fontFamily={"sans-serif"}
              >
                x
              </Box>

              <FormControl isInvalid={isErrorAltura}>
                <VStack>
                  <Input
                    placeholder="A (m)"
                    w={"90px"}
                    boxShadow={"md"}
                    value={inputAltura}
                    onChange={handleInputAlturaValue}
                    type="number"
                    isRequired={"true"}
                  />
                  {!isErrorAltura ? (
                    <FormHelperText>inserir altura</FormHelperText>
                  ) : (
                    <FormErrorMessage>altura obrigatória.</FormErrorMessage>
                  )}
                </VStack>
              </FormControl>
            </HStack>

            <FormLabel
              htmlFor="text"
              ml={"15px"}
              mt={"10px"}
              fontWeight={"bold"}
              w={"320px"}
            >
              Quantas portas tem na área que será pintada :
            </FormLabel>
            <HStack>
              <Input
                w={"300px"}
                ml={"20px"}
                boxShadow={"md"}
                placeholder={"medida padrão 0,80m (l) x 1,90m (a)"}
                value={inputPorta}
                onChange={handleInputPortaValue}
                type="number"
                isRequired={"true"}
              />
            </HStack>
            {!isErrorAlturaParedePorta ? (
              <></>
            ) : (
              <Text fontSize={"15px"} color={"BoxColors.Red"}>
                ops...altura da parede deve ser maior ou igual a 2.2m
              </Text>
            )}
            <FormLabel
              htmlFor="text"
              ml={"15px"}
              mt={"10px"}
              fontWeight={"bold"}
              w={"325px"}
            >
              Quantas janelas tem na área que será pintada :
            </FormLabel>

            <Input
              w={"300px"}
              ml={"5px"}
              boxShadow={"md"}
              placeholder={"medida padrão 2,00m (l) x 1,20m (a)"}
              mb={"20px"}
              value={inputJanela}
              onChange={handleInputJanelaValue}
              type="number"
              isRequired={"true"}
            />
            <Box>
              {!isErrorAreaPortaJanela ? (
                <FormHelperText fontSize={"15px"}>
                  Aréa de janela e portas deve ser max{" "}
                  {(areaParede / 2).toFixed(2)}m2
                </FormHelperText>
              ) : (
                <Text fontSize={"15px"} color={"BoxColors.Red"}>
                  Quantidade de janelas e portas excedeu o limite.
                </Text>
              )}
            </Box>

            <Button
              borderRadius={"0px 0px 10px 10px"}
              bg={"primary.main1"}
              w={"350px"}
              h={"50px"}
              fontSize={"20px"}
              _hover={{
                color: "primary.main",
              }}
              isDisabled={isActive}
              color={isActiveColor}
              fontWeight={"extrabold"}
              onClick={() => (AddParede(), Verify())}
            >
              ADICIONAR PAREDE +
            </Button>
          </FormControl>

          <AvatarAnswer />
        </HStack>

        <VStack>
          <Heading>Resultado</Heading>
          <Text fontSize={"25px"}>Você vai precisar de:</Text>
          <Box
            flexDirection={"column"}
            w={"350px"}
            borderBottom={"1px"}
            borderBottomColor={"gray.100"}
            boxShadow={"lg"}
            borderRadius={"10"}
            mb={"50px"}
            _hover={{
              transform: "translateY(-3px)",
              borderColor: "BoxColors.Yellow",
            }}
            transition="border 0.2s, ease 0s, transform 0.2s"
          >
            <Text fontSize={"25px"} color={"BoxColors.GreenApple"}>
              Área total a ser pintada : {areaParedes.toFixed(2)}m2
            </Text>
            <Text fontSize={"25px"} color={"BoxColors.GreenApple"}>
              Quantiade de tinta : {(areaParedes / 5).toFixed(2)} litros
            </Text>
            {result &&
              result.map((result) => (
                <Text
                  fontSize={"25px"}
                  color={"BoxColors.Green"}
                  fontWeight={"bold"}
                  textAlign={"justify"}
                  ml="100px"
                >
                  {result}
                </Text>
              ))}
          </Box>
          <Tips />
        </VStack>
      </VStack>
    </>
  );
};
