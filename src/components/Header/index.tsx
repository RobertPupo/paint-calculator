import {
  Box,
  Flex,
  VStack,
  HStack,
  Image,
  Button,
  useMediaQuery,
  Menu,
  MenuButton,
  MenuList,
} from "@chakra-ui/react";

/* imagens */

import sofa from "../../assets/images/header/sofa.png";
import mesa from "../../assets/images/header/mesa.png";
import quadro from "../../assets/images/header/retrato.png";
import vaso from "../../assets/images/header/vaso.png";

import { useContext } from "react";
import { ColorsContext } from "../../contexts/Colors";
import { VscChevronDown } from "react-icons/vsc";
import { useState } from "react";

export const Header = () => {
  const [isLargerThan1281] = useMediaQuery("(min-width: 1281px)");

  const { colors } = useContext(ColorsContext);

  const [colorSelected, setColorSelected] = useState("BoxColors.Yellow");

  const TakeColorHeaderBg = (color: string) => {
    setColorSelected(color);
  };

  return (
    <>
      {isLargerThan1281 ? (
        <>
          <Box
            flexDirection="row"
            justifyContent="center"
            alignItems="center"
            px="5"
            py="1"
            bg={colorSelected}
            position="fixed"
            width="100%"
            height="270px"
            zIndex="100"
            boxShadow={"0px 4px 4px rgba(0, 0, 0, 0.25)"}
          >
            <Box
              w="100%"
              h="40px"
              bg={"primary.main"}
              position={"fixed"}
              top={"270px"}
              right={"0px"}
              left={"0px"}
            ></Box>
            <Flex
              flexDirection="row"
              justifyContent="center"
              alignItems="center"
              px="5"
              py="1"
              position="absolute"
              top="-20"
              left="165px"
              width="100%"
              zIndex="100"
              flexDir={"column"}
            >
              <HStack>
                <VStack spacing={"-50px"} alignItems={"center"}>
                  <Image src={quadro} ml={"220px"} w={"250"} h={"125"}></Image>
                  <HStack spacing={"12px"} alignItems={"center"}>
                    <Image
                      src={mesa}
                      alt="mesa"
                      mt={"30px"}
                      height={"200px"}
                      width={"30"}
                    />
                    <Image
                      src={sofa}
                      alt="sofá"
                      height={"200px"}
                      width={"350px"}
                    />
                  </HStack>
                </VStack>
                <Image src={vaso} alt="vaso" height={"500px"} width={"130"} />
                <Menu>
                  <MenuButton
                    as={Button}
                    rightIcon={<VscChevronDown />}
                    w={"250px"}
                    h={"70px"}
                    bg={"gray.600"}
                    borderRadius={"10px"}
                    color={colorSelected}
                    textAlign={"left"}
                    fontSize={"20px"}
                  >
                    {"Simulador de cores"}
                  </MenuButton>

                  <MenuList
                    flexDirection={"row"}
                    w={"200px"}
                    h={"200px"}
                    flexWrap={"wrap"}
                    bg={"gray.100"}
                    ml={"25px"}
                    justifyContent={"flex-start"}
                  >
                    {colors &&
                      colors.map((color) => (
                        <Box
                          as="button"
                          w={"30px"}
                          h={"25px"}
                          _hover={{
                            transform: "translateY(2px)",
                            borderColor: "BoxColors.Red",
                          }}
                          border={"1px"}
                          borderColor={"primary.main1"}
                          bg={color.name}
                          ml="10px"
                          mt="10px"
                          onClick={() => TakeColorHeaderBg(color.name)}
                        />
                      ))}
                  </MenuList>
                </Menu>
              </HStack>
            </Flex>
          </Box>
        </>
      ) : (
        <>
          {/* mobile */}
          <Box
            flexDirection="row"
            justifyContent="center"
            alignItems="center"
            px="5"
            py="1"
            bg={colorSelected}
            position="fixed"
            width="100%"
            height="190px"
            zIndex="100"
            boxShadow={"0px 4px 4px rgba(0, 0, 0, 0.25)"}
          >
            <Box
              w="100%"
              h="40px"
              bg={"primary.main"}
              position={"fixed"}
              top={"190px"}
              right={"0px"}
              left={"0px"}
            ></Box>
            <Flex
              flexDirection="row"
              justifyContent="center"
              alignItems="center"
              px="5"
              py="1"
              position="absolute"
              top="40px"
              width="100%"
              zIndex="100"
              flexDir={"column"}
            >
              <VStack spacing={"-40px"}>
                <HStack spacing={"-80px"}>
                  <Image
                    src={quadro}
                    alt="quadro"
                    mr={"120px"}
                    w={"150"}
                    h={"70"}
                  ></Image>
                  <Menu>
                    <MenuButton
                      mt={"10px"}
                      as={Button}
                      rightIcon={<VscChevronDown />}
                      w={"130px"}
                      h={"30px"}
                      bg={"gray.600"}
                      borderRadius={"10px"}
                      color={colorSelected}
                      textAlign={"left"}
                      fontSize={"10px"}
                    >
                      {"Simulador de cores"}
                    </MenuButton>

                    <MenuList
                      flexDirection={"row"}
                      w={"120px"}
                      h={"180px"}
                      flexWrap={"wrap"}
                      bg={"gray.100"}
                      ml={"25px"}
                      justifyContent={"flex-start"}
                    >
                      {colors &&
                        colors.map((color) => (
                          <Box
                            as="button"
                            w={"30px"}
                            h={"25px"}
                            _hover={{
                              transform: "translateY(2px)",
                              borderColor: "BoxColors.Red",
                            }}
                            border={"1px"}
                            borderColor={"primary.main1"}
                            bg={color.name}
                            ml="10px"
                            mt="10px"
                            onClick={() => TakeColorHeaderBg(color.name)}
                          />
                        ))}
                    </MenuList>
                  </Menu>
                </HStack>

                <HStack spacing={"-10px"} alignItems={"center"}>
                  <Image
                    src={sofa}
                    alt="sofá"
                    height={"100px"}
                    width={"250px"}
                  />
                  <Image src={vaso} alt="vaso" height={"200px"} width={"30"} />
                </HStack>
              </VStack>
            </Flex>
          </Box>
        </>
      )}
    </>
  );
};
