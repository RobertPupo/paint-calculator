import { Box, Flex } from "@chakra-ui/react";

import { Header } from "../../components/Header";

import { PaintCalculator } from "../../components/Calculator";

export const Home = () => {
  return (
    <>
      <Box width="95vw" flexDirection={"column"}>
        <Header />
        <Flex
          flexDirection="column"
          position="relative"
          top="320px"
          left="-10px"
          textAlign={"center"}
          justifyContent="center"
          alignItems="center"
        >
          <PaintCalculator />
        </Flex>
      </Box>
    </>
  );
};
