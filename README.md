<h1>Paint Calculator - Test Front-End</h1>

<h2> Objetivo:</h2>

<p>O objetivo desse projeto é construir uma aplicação simples que exibe uma calculdora de interface amigável onde o usuário tem a possibilidade de com
algumas informações básicas fazer o calculo da quantidade necessária de tinta para
pintar uma determinada quantidade de paredes.</p>

<img src="https://fv9-4.failiem.lv/thumb_show.php?i=m3pgbxma5&view" alt="capa_desktop"/>

<a href="https://paint-calculator-nu.vercel.app/" title="deploy">Deploy da aplicação</a></br>

<a href="https://youtu.be/VhvsgoCZYYA" titlle="demo">Breve Video com demonstração de algumas funcionalidades</a></br>

<a href="https://www.figma.com/file/yVN2kxefmO6jpGbECcl4DE/Paint-Calculator?node-id=0%3A1" title="figma">Figma que criei</a></br>

<h4> Clone do repositório </h4>

- `https://gitlab.com/RobertPupo/paint-calculator`

<h4> Instalando dependencias</h4>

- `yarn install`

<h4> Rodando projeto</h4>

- `yarn start`

<h4> Rodando teste no Cypress</h4>

- `em breve`

<h4> Tecnologias utilizadas</h4>

- `ReactJS`
- `Javascript`
- `Typescript`
- `HTML5`
- `CSS`
- `Chakra UI`
- `Context API`
- `React Router Dom`

<h4> Por que da Stack ?</h4>

<ul>
  <li> React foi utilizado por minha familiaridade com a ferramenta</li>
  <li>Typescript para tipagem que facilita o gerenciamento do projeto aumentando minha performace como desenvolvedor</li>
  <li>Chakra UI é uma poderosa Lib que utilizo muito para estilização das minhas aplicações, ela me ajuda a ganhar tempo e qualidade no trabalho em relação ao Syled Components ou CSS puro</li>
  <li>Context API para centralizar e gerenciar todas informações que eu preciso</li>

</ul>

<h4> Estrutura dos diretórios</h4>

<img src="https://fv9-6.failiem.lv/thumb_show.php?i=6h2eg8fcy&view" alt="diretorios"/>

-     src
      +---------assests
                +-------images : nesse diretório estão todas as imagens uitlizadas na aplicação.

      +----------components : nesse diretório estão todos componentes utilizados na palicação.

      +----------contexts: nesse diretório estão centralizadas todas as variáveis da aplicação.

      +----------pages: nesse diretório estão todas as páginas da aplicação.

      +----------routes: nesse direitório estão todas as rotas das páginas da aplicação.

      +----------style: nesse diretório está localizado o Theme que trás todo padrão de estilização da aplicação.
